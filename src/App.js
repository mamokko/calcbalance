import React, {Component} from 'react';
import Operation from "./components/operation/Operation";
import History from "./components/history/History";
import Total from "./components/total/Total";

class App extends Component {

  state = {
    transactions: JSON.parse(localStorage.getItem('calcBalance')) || [],
    description: '',
    amount: '',

    resultIncome: 0,
    resultExpense: 0,
    totalBalance: 0,
  };

  componentDidMount() {
    this.getBalance();
  }

  addTransaction = (isAdd) => {
    const transactions = [
        ...this.state.transactions,
      {
        id: `cmr${(+new Date()).toString(16)}`,
        description: this.state.description,
        amount: +this.state.amount,
        isAdd
      }
    ];

    this.setState({
      transactions,
      description: '',
      amount: ''
    }, () => {
        this.getBalance();
        this.addStordge();
      })
  };

  isChangeDescription = ( e ) => this.setState({ description: e.target.value });
  isChangeAmount = ( e ) => this.setState({ amount: e.target.value });

  isDeleteItem = (key) => {
    const transactions = this.state.transactions.filter( item => item.id !== key);
    this.setState({ transactions }, () => this.getBalance());
  };

  getIncome = () => {
    return this.state.transactions
        .reduce(( acc, item ) => item.isAdd ? item.amount + acc : acc, 0 );

  };
  getExpense = () => {
    return this.state.transactions
        .reduce(( acc, item ) => !item.isAdd ? item.amount + acc : acc, 0 );
  };
  getBalance = () => {
    const resultIncome = this.getIncome();
    const resultExpense = this.getExpense();
    const totalBalance = resultIncome - resultExpense;
    this.setState({
      resultIncome,
      resultExpense,
      totalBalance
    })
  };

  addStordge() { localStorage.setItem('calcBalance', JSON.stringify( this.state.transactions )) }

  render() {

    return (
        <div className="App">
          <header>
            <h1>Кошелек</h1>
            <h2>Калькулятор расходов</h2>
          </header>

          <main>
            <div className="container">

              <Total
                  income={ this.state.resultIncome }
                  expense={ this.state.resultExpense }
                  total={ this.state.totalBalance }
              />

              <History
                  transactions={ this.state.transactions }
                  isDeleteItem={ this.isDeleteItem }
              />

              <Operation
                description={ this.state.description }
                amount={ this.state.amount }
                isChangeDescription={ this.isChangeDescription }
                isChangeAmount={ this.isChangeAmount }
                addTransaction={ this.addTransaction }
              />

            </div>
          </main>
        </div>
    );
  }
}

export default App;
