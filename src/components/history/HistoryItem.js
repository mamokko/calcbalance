import React from "react";

const HistoryItem = ( {transaction, isDeleteItem} ) => (
    <li
        className={`history__item history__item-${ transaction.isAdd ? 'plus' : 'minus'}`}>
        { transaction.description }
        <span className="history__money">{transaction.isAdd ? '+ ' : '- '}{ transaction.amount } ₽</span>
        <button
            className="history__delete"
            onClick={ () => isDeleteItem( transaction.id )}
        >
            x
        </button>
    </li>
);

export default HistoryItem;
